package steve.android.flshowcase

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_layout.view.*
import timber.log.Timber

/**
 * @author Steve
 * @since 2/6/18
 */
class FoodAdapter(private var listFood: List<String>) : RecyclerView.Adapter<FoodAdapter.FoodVH>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodVH {
        val inflater = LayoutInflater.from(parent.context)
        return FoodVH(inflater.inflate(R.layout.item_layout, parent, false))
    }

    override fun getItemCount(): Int = listFood.size

    override fun onBindViewHolder(holder: FoodVH?, position: Int) {
        holder?.bind(listFood[position])
    }

    inner class FoodVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                Timber.d("click on item $adapterPosition")
            }
        }

        fun bind(link: String) {
            Glide.with(itemView.context)
                    .load(link)
                    .into(itemView.ivItem)
        }
    }
}