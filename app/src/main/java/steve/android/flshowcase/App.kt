package steve.android.flshowcase

import android.support.multidex.MultiDexApplication
import steve.android.flshowcase.BuildConfig.*
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric


/**
 * @author Steve
 * @since 2/5/18
 */
class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

        if (FLAVOR == "prod") {
            Fabric.with(this, Crashlytics())
        }
    }
}