package steve.android.flshowcase

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val listFood = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initList()

        rvList.apply {
            adapter = FoodAdapter(listFood)
            layoutManager = GridLayoutManager(context, 2)
        }
    }

    private fun initList() {
        listFood.apply {
            add("https://images.pexels.com/photos/461198/pexels-photo-461198.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/5938/food-salad-healthy-lunch.jpg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/376464/pexels-photo-376464.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/710916/pexels-photo-710916.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/76093/pexels-photo-76093.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/247685/pexels-photo-247685.png?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/46239/salmon-dish-food-meal-46239.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/8500/food-dinner-pasta-spaghetti-8500.jpg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/262918/pexels-photo-262918.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/262959/pexels-photo-262959.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/769289/pexels-photo-769289.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/406152/pexels-photo-406152.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/5928/salad-healthy-diet-spinach.jpg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/461198/pexels-photo-461198.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/5938/food-salad-healthy-lunch.jpg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/376464/pexels-photo-376464.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/710916/pexels-photo-710916.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/76093/pexels-photo-76093.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/247685/pexels-photo-247685.png?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/46239/salmon-dish-food-meal-46239.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/8500/food-dinner-pasta-spaghetti-8500.jpg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/262918/pexels-photo-262918.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/262959/pexels-photo-262959.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/769289/pexels-photo-769289.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/406152/pexels-photo-406152.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/5928/salad-healthy-diet-spinach.jpg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/461198/pexels-photo-461198.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/5938/food-salad-healthy-lunch.jpg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/376464/pexels-photo-376464.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/710916/pexels-photo-710916.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/76093/pexels-photo-76093.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/247685/pexels-photo-247685.png?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/46239/salmon-dish-food-meal-46239.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/8500/food-dinner-pasta-spaghetti-8500.jpg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/262918/pexels-photo-262918.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/262959/pexels-photo-262959.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/769289/pexels-photo-769289.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/406152/pexels-photo-406152.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/5928/salad-healthy-diet-spinach.jpg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/461198/pexels-photo-461198.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/5938/food-salad-healthy-lunch.jpg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/376464/pexels-photo-376464.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/710916/pexels-photo-710916.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/76093/pexels-photo-76093.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/247685/pexels-photo-247685.png?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/46239/salmon-dish-food-meal-46239.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/8500/food-dinner-pasta-spaghetti-8500.jpg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/262918/pexels-photo-262918.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/262959/pexels-photo-262959.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/769289/pexels-photo-769289.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/406152/pexels-photo-406152.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/5928/salad-healthy-diet-spinach.jpg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/461198/pexels-photo-461198.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/5938/food-salad-healthy-lunch.jpg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/376464/pexels-photo-376464.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/710916/pexels-photo-710916.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/76093/pexels-photo-76093.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/247685/pexels-photo-247685.png?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/46239/salmon-dish-food-meal-46239.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/8500/food-dinner-pasta-spaghetti-8500.jpg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/262918/pexels-photo-262918.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/262959/pexels-photo-262959.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/769289/pexels-photo-769289.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/406152/pexels-photo-406152.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/5928/salad-healthy-diet-spinach.jpg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/461198/pexels-photo-461198.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/5938/food-salad-healthy-lunch.jpg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/376464/pexels-photo-376464.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/710916/pexels-photo-710916.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/76093/pexels-photo-76093.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/247685/pexels-photo-247685.png?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/46239/salmon-dish-food-meal-46239.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/8500/food-dinner-pasta-spaghetti-8500.jpg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/262918/pexels-photo-262918.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/262959/pexels-photo-262959.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/769289/pexels-photo-769289.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/406152/pexels-photo-406152.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb")
            add("https://images.pexels.com/photos/5928/salad-healthy-diet-spinach.jpg?h=350&dpr=2&auto=compress&cs=tinysrgb")
        }
    }
}
